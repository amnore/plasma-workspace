/*
    SPDX-FileCopyrightText: 2017 David Edmundson <davidedmundson@kde.org>
    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "qmenu.h"
#include <QApplication>

#include <QDateTime>
#include <QDebug>
#include <QIcon>
#include <QMainWindow>
#include <QMenuBar>
#include <QtWidgets>

#include <functional>
#include <tuple>

class MainWindow : public QMainWindow
{
public:
    MainWindow();
};

MainWindow::MainWindow()
    : QMainWindow()
{
    /*set an initial menu with the following
    Menu A
      - Item
      - Checkable Item
      - Item With Icon
      - A separator
      - Menu B
         - Item B1
     Menu C
      - DynamicItem ${timestamp}

      TopLevelItem

      Also create three buttons which add an item to
      Menu A, Menu B and the top level menu when clicked, respectively
    */
    auto widget = new QWidget();
    auto layout = new QVBoxLayout();
    setCentralWidget(widget);
    widget->setLayout(layout);

    auto addDynItem = [](auto menu) {
        menu->addAction("Dynamic Item " + QDateTime::currentDateTime().toString());
    };

    QAction *t;
    auto menuA = new QMenu("Menu A", this);
    menuA->addAction("Item");

    t = menuA->addAction("Checkable Item");
    t->setCheckable(true);

    t = menuA->addAction(QIcon::fromTheme("document-edit"), "Item with icon");

    menuA->addSeparator();

    auto menuB = new QMenu("Menu B", this);
    menuB->addAction("Item B1");
    menuA->addMenu(menuB);

    menuBar()->addMenu(menuA);

    auto menuC = new QMenu("Menu C", this);
    connect(menuC, &QMenu::aboutToShow, [=] {
        menuC->clear();
        addDynItem(menuC);
    });

    menuBar()->addMenu(menuC);

    menuBar()->addAction("Top Level Item");

    std::tuple<const char *, std::function<void()>> arr[] = {
        {"Add to menu A", std::bind(addDynItem, menuA)},
        {"Add to menu B", std::bind(addDynItem, menuB)},
        {"Add to top level menu", std::bind(addDynItem, menuBar())}
    };

    for (auto t : arr) {
        auto button = new QPushButton(std::get<0>(t));
        connect(button, &QAbstractButton::clicked, std::get<1>(t));
        layout->addWidget(button);
    }
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    MainWindow mw;
    mw.show();
    return app.exec();
}
